# F-Droid Server CI image

**ARCHIVED!** This is no longer needed, instead try:
* https://gitlab.com/fdroid/ci-images-base
* https://gitlab.com/fdroid/ci-images-client

This Docker image was used in
[fdroidserver](https://gitlab.com/fdroid/fdroidserver)'s continuous
integration via Gitlab.  It is built on top of our
[ci-images-base](https://gitlab.com/fdroid/ci-images-base) Docker
image.  It includes stuff that only the server tests need, like Python
linters.
