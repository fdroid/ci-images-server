FROM registry.gitlab.com/fdroid/ci-images-base:20180111
MAINTAINER team@f-droid.org

RUN printf "Package: androguard python3-asn1crypto\nPin: release a=stretch-backports\nPin-Priority: 500\n" \
		> /etc/apt/preferences.d/debian-stretch-backports.pref \
	&& echo "deb http://deb.debian.org/debian/ stretch-backports main" > /etc/apt/sources.list.d/backports.list \
	&& apt-get update && apt-get -qy dist-upgrade \
	&& apt-get install -qy --no-install-recommends \
		androguard/stretch-backports \
		python3-asn1crypto/stretch-backports \
		gcc \
		git \
		gnupg \
		libjpeg-dev \
		libffi-dev \
		libssl-dev \
		make \
		python3-babel \
		python3-dev \
		python3-defusedxml \
		python3-pip \
		python3-ruamel.yaml \
		python3-setuptools \
		python3-venv \
		rsync \
		ruby \
		zlib1g-dev \
		`apt-cache depends fdroidserver | grep -Fv -e java -e jdk -e '<' | awk '/Depends:/{print$2}'` \
	&& apt-get -qy autoremove --purge \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

COPY test /
